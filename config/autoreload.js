module.exports.autoreload = {
  active: true,
  usePolling: false,
  dirs: [
    "api/models",
    "api/controllers",
    "api/helpers",
    "api/policies",
    "config/"

  ],
  ignored: [
    // Ignore all files with .ts extension
    "**.ts"
  ]
};