/**
 * Seed Function
 * (sails.config.bootstrap)
 *
 * A function that runs just before your Sails app gets lifted.
 * > Need more flexibility?  You can also create a hook.
 *
 * For more information on seeding your app with fake data, check out:
 * https://sailsjs.com/config/bootstrap
 */

module.exports.bootstrap = async function () {

  // By convention, this is a good place to set up fake data during development.
  //
  // For example:
  // ```
  // // Set up fake development data (or if we already have some, avast)
  // if (await User.count() > 0) {
  //   return;
  // }
  //
  await Users.createEach([{
      login: '380661234567',
      password: '123',
      name: 'Admin',
      position: '1'
    },
    {
      login: '380661234568',
      password: '123',
      name: 'Vasya1'
    },
  ]);

  await Courses.createEach([{
      title: 'Course1',
      description: 'Разработка и продвижение сайтов – это очень востребованные услуги в наше время. Следуя поговорке: «Если вас нет в Интернете, то вас не существует», и компании, и люди хотят быть представлены в сети, в этом Вам могут помочь курсы по продвижению сайтов в Кривом Роге. Курсы веб разработки сайтов позволят всегда найти работу, причем хорошо оплачиваемую – в среднем 1500 у.е. в месяц. ',
      logo: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRBmN3P7_P1rabaJrP_G8OUukAno6JHX7IcRkHz3gtQd0CRXnF29Q'
    },
    {
      title: 'Course2',
      description: 'descriptions2',
      logo: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRKWWQHwJKxHxvjoB1GTMggSRf2iw9sMd5FSyR92yoBmvxXgRzWfw'
    },
    {
      title: 'Course3',
      description: 'descriptions3',
      logo: "https://www.freeiconspng.com/uploads/dice-png-transparent-index-of-images-26.png"
    },
    {
      title: 'Course4',
      description: 'descriptions2'
    },
    {
      title: 'Course5',
      description: 'descriptions3'
    },
    {
      title: 'Course6',
      description: 'descriptions2',
    },
    {
      title: 'Course7',
      description: 'descriptions3',
    },
    {
      title: 'Course8',
      description: 'descriptions2',
    }
  ]);
  await Lessons.createEach([{
      title: 'Lesson1',
      description: 'descriptions1',
      course_id: 1
    },
    {
      title: 'Lesson2',
      description: 'descriptions2',
      course_id: 1
    },
    {
      title: 'Lesson3',
      description: 'descriptions3',
      course_id: 1
    },
    {
      title: 'Lesson4',
      description: 'descriptions2',
      course_id: 1
    },
    {
      title: 'Lesson5',
      description: 'descriptions3',
      course_id: 1
    },
    {
      title: 'Lesson1',
      description: 'descriptions2',
      course_id: 2
    },
    {
      title: 'Lesson2',
      description: 'descriptions3',
      course_id: 2
    },
    {
      title: 'Lesson3',
      description: 'descriptions2',
      course_id: 2
    },
    {
      title: 'Lesson1',
      description: 'descriptions3',
      course_id: 3
    },
    {
      title: 'Lesson2',
      description: 'descriptions2',
      course_id: 3
    },
    {
      title: 'Lesson1',
      description: 'descriptions3',
      course_id: 4
    },
    {
      title: 'Lesson1',
      description: 'descriptions2',
      course_id: 5
    },
    {
      title: 'Lesson1',
      description: 'descriptions3',
      course_id: 6
    },
  ]);

  await Contents.createEach([{
      data: `<p>
      <span style="font-size: 18px;">Quill Rich Text Editor</span>
  </p>
  <p>
      <br>
  </p>
  <p>Quill is a free,
      <a href="https://github.com/quilljs/quill/" target="_blank">open source</a>WYSIWYG editor built for the modern web. With its
      <a href="http://quilljs.com/docs/modules/" target="_blank">extensible architecture</a>and a
      <a href="http://quilljs.com/docs/api/" target="_blank">expressive API</a>you can completely customize it to fulfill your needs. Some built in features include:</p>
  <p>
      <br>
  </p>
  <ul>
      <li>Fast and lightweight</li>
      <li>Semantic markup</li>
      <li>Standardized HTML between browsers</li>
      <li>Cross browser support including Chrome, Firefox, Safari, and IE 9+</li>
  </ul>
  <iframe class="ql-video" frameborder="0" allowfullscreen="true" src="https://www.youtube.com/embed/UbIci4Pye6w?showinfo=0"></iframe>
  <p>
      <br>
  </p>
  <p>
      <span style="font-size: 18px;">Downloads</span>
  </p>
  <p>
      <br>
  </p>
  <ul>
      <li>
          <a href="https://quilljs.com" target="_blank">Quill.js</a>, the free, open source WYSIWYG editor</li>
      <li>
          <a href="https://zenoamaro.github.io/react-quill" target="_blank">React-quill</a>, a React component that wraps Quill.js</li>
  </ul>`,
      lesson_id: 1
    },
    {
      data: 'Content2  Content2  Content2  Content2',
      lesson_id: 1
    },
    {
      data: 'Content3  Content3  Content3  Content3',
      lesson_id: 1
    },
    {
      data: 'Content4  Content4  Content4  Content4',
      lesson_id: 1
    }
  ]);
  // ```
};