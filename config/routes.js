/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /***************************************************************************
   *                                                                          *
   * Make the view located at `views/homepage.ejs` your home page.            *
   *                                                                          *
   * (Alternatively, remove this and add an `index.html` file in your         *
   * `assets` directory)                                                      *
   *                                                                          *
   ***************************************************************************/

  // '/': { view: 'pages/homepage' },

  'get /me': 'UsersController.getAboutMe',
  'post /user': 'UsersController.create',
  'post /login': 'AuthController.login',

  'get /dashboard/courses': 'CoursesDashboardController.getAll',
  'get /dashboard/course/:courseID': 'CoursesDashboardController.get',
  'post /dashboard/course': 'CoursesDashboardController.create',
  'put /dashboard/course/:courseID': 'CoursesDashboardController.update',
  'delete /dashboard/course/:courseID': 'CoursesDashboardController.delete',
  'get /courses': 'CoursesController.getAll',
  'get /course/:courseID': 'CoursesController.get',


  'get /dashboard/lessons': 'LessonsDashboardController.getAll',
  'get /dashboard/lesson/:lessonID': 'LessonsDashboardController.get',
  'post /dashboard/lesson': 'LessonsDashboardController.create',
  'put /dashboard/lesson/:lessonID': 'LessonsDashboardController.update',
  'delete /dashboard/lesson/:lessonID': 'LessonsDashboardController.delete',
  'get /lesson/:lessonID': 'LessonsController.get',

  'get /dashboard/contents': 'ContentsDashboardController.getContents',
  'post /dashboard/content': 'ContentsDashboardController.create',
  'put /dashboard/content/:contentID': 'ContentsDashboardController.update',
  'delete /dashboard/content/:contentID': 'ContentsDashboardController.delete'


  /***************************************************************************
   *                                                                          *
   * More custom routes here...                                               *
   * (See https://sailsjs.com/config/routes for examples.)                    *
   *                                                                          *
   * If a request to a URL doesn't match any of the routes in this file, it   *
   * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
   * not match any of those, it is matched against static assets.             *
   *                                                                          *
   ***************************************************************************/


};