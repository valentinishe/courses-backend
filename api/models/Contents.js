/**
 * Contents.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  primaryKey: 'id',

  attributes: {
    id: {
      type: 'number',
      autoIncrement: true
    },
    data: {
      type: 'json',
      required: true
    },
    // 0 - enabled, 1 - disabled
    isDisabled: {
      type: 'number',
      defaultsTo: 0,
    },
    lesson_id: {
      model: 'lessons'
    },
  }

};