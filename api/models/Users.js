/**
 * Users.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  primaryKey: 'id',

  attributes: {
    id: {
      autoIncrement: true,
      type: 'number',
    },
    // mobile phone
    login: {
      type: 'string',
      unique: true,
      required: true,
      maxLength: 12,
      minLength: 12
    },
    password: {
      type: 'string',
      required: true
    },
    name: {
      type: 'string',
      required: true
    },
    // 0 - learner, 1 - mentor, 2 - admin
    position: {
      type: 'number',
      defaultsTo: 0,
    },
    // 0 - enabled, 1 - disabled
    isDisabled: {
      type: 'number',
      defaultsTo: 0,
    }
  }
};