/**
 * Courses.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  primaryKey: 'id',

  attributes: {
    id: {
      type: 'number',
      autoIncrement: true
    },
    title: {
      type: 'string',
      required: true
    },
    description: {
      type: 'json',
      required: true
    },
    logo: {
      type: 'string',
      defaultsTo: ''
    },
    // 0 - enabled, 1 - disabled
    isDisabled: {
      type: 'number',
      defaultsTo: 0,
    },
    lessons: {
      collection: 'lessons',
      via: 'course_id'
    }
  }
};