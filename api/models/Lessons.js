/**
 * Lessons.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  primaryKey: 'id',

  attributes: {
    id: {
      type: 'number',
      autoIncrement: true
    },
    title: {
      type: 'string',
      required: true
    },
    description: {
      type: 'string',
      required: true
    },
    // 0 - enabled, 1 - disabled
    isDisabled: {
      type: 'number',
      defaultsTo: 0,
    },
    course_id: {
      model: 'courses'
    },
    contents: {
      collection: 'contents',
      via: 'lesson_id'
    }
  }

};