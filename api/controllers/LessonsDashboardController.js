/**
 * LessonsDashboardController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  // GET
  getAll: async (req, res) => {

    const lessons = await Lessons.find().then(res => res);

    return res.Collection({
      data: lessons.map(l => ({
        id: l.id,
        title: l.title,
        description: l.description,
        course_id: l.course_id
      })),
      limit: req.query.limit,
      page: req.query.page,
    });
  },
  get: async (req, res) => {

    const lesson = await Lessons.findOne({
      id: req.params.lessonID
    }).populate('contents');

    if (!lesson) {
      return res.notFound();
    }

    return res.Item({
      data: {
        id: lesson.id,
        title: lesson.title,
        description: lesson.description,
        course_id: lesson.course_id,
        contents: lesson.contents.map(c => ({
          lesson_id: c.lesson_id,
          data: c.data,
          id: c.id
        }))
      }
    });
  },

  // POST
  create: async (req, res) => {

    const validationMethods = await sails.helpers.validationMethods();
    const data = req.body;

    const validationMap = {
      methods: {
        title: {
          required: validationMethods.isRequired
        },
        description: {
          required: validationMethods.isRequired,
        },
        course_id: {
          required: validationMethods.isRequired,
          isNumber: validationMethods.isNumber,
        }
      },
      messages: {
        title: {
          required: sails.__('core.requiredField')
        },
        description: {
          required: sails.__('core.requiredField'),
        },
        course_id: {
          required: sails.__('core.requiredField'),
          isNumber: sails.__('core.onlyNumber'),
        }
      }
    }


    const Errors = await sails.helpers.onCheckValidation.with({
      data,
      validationMap
    });

    if (Errors) {
      return res.json(Errors);
    }

    try {
      const newLesson = await Lessons.create({
          description: data.description,
          title: data.title,
          course_id: data.course_id
        })
        .fetch();
      return res.Item({
        data: {
          id: newLesson.id,
          title: newLesson.title,
          description: newLesson.description,
          course_id: newLesson.course_id
        }
      });


    } catch (e) {
      const ErrorsDB = await sails.helpers.onCheckDbErrors.with({
        data: e
      });
      return res.json(ErrorsDB);
    }
  },

  // PUT
  update: async function (req, res) {

    const validationMethods = await sails.helpers.validationMethods();
    const data = req.body;
    const validationMap = {
      methods: {
        title: {
          required: validationMethods.isRequired
        },
        description: {
          required: validationMethods.isRequired,
        },
        course_id: {
          required: validationMethods.isRequired,
          isNumber: validationMethods.isNumber,
        }
      },
      messages: {
        title: {
          required: sails.__('core.requiredField')
        },
        description: {
          required: sails.__('core.requiredField'),
        },
        course_id: {
          required: sails.__('core.requiredField'),
          isNumber: sails.__('core.onlyNumber'),
        }
      }
    }


    const Errors = await sails.helpers.onCheckValidation.with({
      data,
      validationMap
    });

    if (Errors) {
      return res.json(Errors);
    }

    try {
      const updatedLesson = await Lessons.updateOne({
          id: req.params.lessonID || 0
        })
        .set({
          description: data.description,
          title: data.title,
          course_id: data.course_id
        });

      if (!updatedLesson) {
        return res.notFound();
      }
      return res.Item({
        data: {
          id: updatedLesson.id,
          title: updatedLesson.title,
          description: updatedLesson.description,
          course_id: updatedLesson.course_id
        }
      });
    } catch (e) {
      const ErrorsDB = await sails.helpers.onCheckDbErrors.with({
        data: e
      });
      return res.json(ErrorsDB);
    }
  },

  // DELETE
  delete: async function (req, res) {
    const isDeleted = await Lessons.destroyOne({
      id: req.params.lessonID || 0
    });
    if (isDeleted) {
      return res.send({
        code: 204
      });
    }
    return res.notFound()
  }
};