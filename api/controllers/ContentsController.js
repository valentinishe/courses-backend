/**
 * ContentsDashboardController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  // GET
  getContents: async (req, res) => {
    return res.Collection({
      data: await Contents.find().then(res => res),
      limit: req.query.limit,
      page: req.query.page,
    });
  },

  // POST
  create: async (req, res) => {
    const validationMethods = await sails.helpers.validationMethods();
    const data = req.body;
    const validationMap = {
      methods: {
        data: {
          required: validationMethods.isRequired
        },
        lesson_id: {
          required: validationMethods.isRequired,
          isNumber: validationMethods.isNumber,
        },
      },
      messages: {
        data: {
          required: sails.__('core.requiredField')
        },
        lesson_id: {
          required: sails.__('core.requiredField'),
          isNumber: sails.__('core.onlyNumber'),
        }
      }
    }


    const Errors = await sails.helpers.onCheckValidation.with({
      data,
      validationMap
    });

    if (Errors) {
      return res.json(Errors);
    }

    try {
      const newContent = await Contents.create({
          data: data.data,
          lesson_id: data.lesson_id
        })
        .fetch();
      return res.Item({
        data: newContent
      });


    } catch (e) {
      const ErrorsDB = await sails.helpers.onCheckDbErrors.with({
        data: e
      });
      return res.json(ErrorsDB);
    }
  },

  // PUT
  update: async function (req, res) {

    const validationMethods = await sails.helpers.validationMethods();
    const data = req.body;
    const validationMap = {
      methods: {
        data: {
          required: validationMethods.isRequired
        },
        lesson_id: {
          required: validationMethods.isRequired,
          isNumber: validationMethods.isNumber,
        },
      },
      messages: {
        data: {
          required: sails.__('core.requiredField')
        },
        lesson_id: {
          required: sails.__('core.requiredField'),
          isNumber: sails.__('core.onlyNumber'),
        },
      }
    }

    const Errors = await sails.helpers.onCheckValidation.with({
      data,
      validationMap
    });

    if (Errors) {
      return res.json(Errors);
    }

    try {
      const updatedContent = await Contents.updateOne({
          id: req.params.contentID || 0
        })
        .set({
          data: data.data,
          lesson_id: data.lesson_id
        });

      if (!updatedContent) {
        return res.notFound();
      }


      return res.Item({
        data: updatedContent
      });
    } catch (e) {
      const ErrorsDB = await sails.helpers.onCheckDbErrors.with({
        data: e
      });
      return res.json(ErrorsDB);
    }
  },

  // DELETE
  delete: async function (req, res) {
    const isDeleted = await Contents.destroyOne({
      id: req.params.contentID || 0
    });
    if (isDeleted) {
      return res.send({
        code: 204
      });
    }
    return res.notFound({
      code: 404
    })
  }

};