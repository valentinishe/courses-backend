/**
 * CourseDashboardController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  // GET


  getAll: async (req, res) => {

    const courses = await Courses.find();


    return res.Collection({
      data: courses.map(c => ({
        id: c.id,
        title: c.title,
        description: c.description,
        logo: c.logo
      })),
      limit: req.query.limit,
      page: req.query.page,
    });
  },
  get: async (req, res) => {

    const course = await Courses.findOne({
      id: req.params.courseID
    }).populate('lessons');

    if (!course) {
      return res.notFound();
    }

    return res.Item({
      data: {
        id: course.id,
        title: course.title,
        description: course.description,
        logo: course.logo,
        lessons: course.lessons.map(l => ({
          course_id: l.course_id,
          description: l.description,
          title: l.title,
          id: l.id
        }))
      }
    });
  },

};