/**
 * LessonsDashboardController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  // GET
  getAll: async (req, res) => {

    const Lessons = await Lessons.find().then(res => res);

    return res.Collection({
      data: Lessons.map(l => ({
        id: l.id,
        title: l.title,
        description: l.description,
        course_id: l.course_id
      })),
      limit: req.query.limit,
      page: req.query.page,
    });
  },
  get: async (req, res) => {


    const Lesson = await Lessons.findOne({
      id: req.params.lessonID
    }).populate('contents');


    if (!Lesson) {
      return res.notFound();
    }

    return res.Item({
      data: {
        id: Lesson.id,
        title: Lesson.title,
        description: Lesson.description,
        course_id: Lesson.course_id,
        contents: Lesson.contents.map(c => ({
          lesson_id: c.lesson_id,
          data: c.data,
          id: c.id
        }))
      }
    });
  },


};