/**
 * CourseDashboardController
 * gggg 
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  // GET
  getAll: async (req, res) => {

    const courses = await Courses.find().then(res => res);

    return res.Collection({
      data: courses.map(c => ({
        id: c.id,
        title: c.title,
        description: c.description,
        logo: c.logo
      })),
      limit: req.query.limit,
      page: req.query.page,
    });
  },
  get: async (req, res) => {

    const course = await Courses.findOne({
      id: +req.params.courseID
    }).populate('lessons');

    if (!course) {
      return res.notFound();
    }

    return res.Item({
      data: {
        id: course.id,
        title: course.title,
        description: course.description,
        logo: course.logo,
        lessons: course.lessons.map(l => ({
          course_id: l.course_id,
          description: l.description,
          title: l.title,
          id: l.id
        }))
      }
    });
  },

  // POST
  create: async (req, res) => {

    const validationMethods = await sails.helpers.validationMethods();
    const data = req.body;
    const validationMap = {
      methods: {
        title: {
          required: validationMethods.isRequired
        },
        description: {
          required: validationMethods.isRequired,
        }
      },
      messages: {
        title: {
          required: sails.__('core.requiredField')
        },
        description: {
          required: sails.__('core.requiredField'),
        }
      }
    }


    const Errors = await sails.helpers.onCheckValidation.with({
      data,
      validationMap
    });

    if (Errors) {
      return res.json(Errors);
    }

    try {
      const newCourse = await Courses.create({
          description: data.description,
          title: data.title
        })
        .fetch();
      return res.Item({
        data: {
          id: newCourse.id,
          title: newCourse.title,
          description: newCourse.description,
          logo: newCourse.logo
        }
      });


    } catch (e) {
      const ErrorsDB = await sails.helpers.onCheckDbErrors.with({
        data: e
      });
      return res.json(ErrorsDB);
    }
  },

  // PUT
  update: async function (req, res) {
    const validationMethods = await sails.helpers.validationMethods();
    const data = req.body;
    const validationMap = {
      methods: {
        title: {
          required: validationMethods.isRequired
        },
        description: {
          required: validationMethods.isRequired,
        }
      },
      messages: {
        title: {
          required: sails.__('core.requiredField')
        },
        description: {
          required: sails.__('core.requiredField'),
        }
      }
    }


    const Errors = await sails.helpers.onCheckValidation.with({
      data,
      validationMap
    });

    if (Errors) {
      return res.json(Errors);
    }

    try {
      const updatedCourse = await Courses.updateOne({
          id: req.params.courseID || 0
        })
        .set({
          description: data.description,
          title: data.title
        });

      if (!updatedCourse) {
        return res.notFound();
      }
      return res.Item({
        data: {
          id: updatedCourse.id,
          title: updatedCourse.title,
          description: updatedCourse.description,
          logo: updatedCourse.logo
        }
      });
    } catch (e) {
      const ErrorsDB = await sails.helpers.onCheckDbErrors.with({
        data: e
      });
      return res.json(ErrorsDB);
    }
  },

  // DELETE
  delete: async function (req, res) {
    const isDeleted = await Courses.destroyOne({
      id: req.params.courseID || 0
    });

    if (isDeleted) {
      return res.send({
        code: 204
      });
    }
    return res.notFound()
  }
};