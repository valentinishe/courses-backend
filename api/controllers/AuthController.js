/**
 * AuthController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  // POST
  login: async (req, res) => {

    const validationMethods = await sails.helpers.validationMethods();
    const JWT = await sails.helpers.jwt();
    const data = req.body;
    const validationMap = {
      methods: {
        login: {
          required: validationMethods.isRequired,
          isLength: validationMethods.isLength(12, 12)
        },
        password: {
          required: validationMethods.isRequired
        }
      },
      messages: {
        login: {
          required: sails.__('core.requiredField'),
          isLength: sails.__('core.isLength').replace('{n}', 12)
        },
        password: {
          required: sails.__('core.requiredField')
        }
      }
    }


    //validation block
    const Errors = await sails.helpers.onCheckValidation.with({
      data,
      validationMap
    });

    if (Errors) {
      return res.json(Errors);
    }

    try {
      // search user
      const user = await Users
        .findOne({
          login: data.login,
          password: data.password
        });

      if (user) { // genarate tokens if user exist
        const accessToken = JWT.createToken({
          payload: {
            id: user.id,
            login: user.login
          },
          secret: sails.config.session.secretAccess,
          expiresIn: sails.config.session.expiresInAccess
        });
        const refreshToken = JWT.createToken({
          payload: {
            id: user.id,
            login: user.login
          },
          secret: sails.config.session.secretRefresh,
          expiresIn: sails.config.session.expiresInRefresh
        });

        return res.send({
          accessToken,
          refreshToken,
          expiresIn: sails.config.session.expiresInAccess
        });
      }

      return res.send({
        error: true,
        code: 401,
        message: sails.__('core.notIsLoggedIn')
      });

    } catch (e) {
      const ErrorsDB = await sails.helpers.onCheckDbErrors.with({
        data: e
      });
      return res.json(ErrorsDB);
    }
  },


};