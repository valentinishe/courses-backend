/**
 * UsersController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */


module.exports = {
  // GET
  getAboutMe: async (req, res) => {
    const user = await Users.findOne({
      id: req.userID
    });
    return res.Item({
      data: {
        id: user.id,
        position: user.position,
        name: user.name
      }
    });
  },

  // POST
  create: async (req, res) => {

    const validationMethods = await sails.helpers.validationMethods();
    const data = req.body;
    const validationMap = {
      methods: {
        name: {
          required: validationMethods.isRequired
        },
        login: {
          required: validationMethods.isRequired,
          isLength: validationMethods.isLength(12, 12)
        },
        password: {
          required: validationMethods.isRequired
        }
      },
      messages: {
        name: {
          required: sails.__('core.requiredField')
        },
        login: {
          required: sails.__('core.requiredField'),
          isLength: sails.__('core.isLength').replace('{n}', 12)
        },
        password: {
          required: sails.__('core.requiredField')
        }
      }
    }


    const Errors = await sails.helpers.onCheckValidation.with({
      data,
      validationMap
    });

    if (Errors) {
      return res.json(Errors);
    }

    try {
      const newUser = await Users
        .create({
          login: data.login,
          name: data.name,
          password: data.password,
          position: 0
        })
        .fetch();

      return res.send(newUser);


    } catch (e) {
      const ErrorsDB = await sails.helpers.onCheckDbErrors.with({
        data: e
      });
      return res.json(ErrorsDB);
    }
  },


  // // PUT
  // updateCard: async function (req, res) {
  //     const updateCard  = await Card
  //         .update({ id: req.params.id })
  //         .set({...req.body})
  //         .fetch();
  //     return res.send(updateCard);
  // },

  // // DELETE
  // deleteCard: async function (req, res) {
  //     await Card.destroy({ id: req.params.id });
  //     return res.send({ code: 204 });
  // }
};