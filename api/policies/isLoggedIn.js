// policies/isLoggedIn.js
module.exports = async function (req, res, proceed) {

  const token = _.get(req, 'headers.authorization', false)
  const JWT = await sails.helpers.jwt();
  const secret = sails.config.session.secretAccess;

  // mentor/admin regexp
  RegExp = /^\/dashboard/g;


  if (token) {
    const payload = await JWT.verifyToken({
      token,
      secret
    });


    switch (payload) {
      case null:
        return res.ExpiredToken();
      case false:
        return res.Forbidden();
    }


    // check additional access for mentor / admin
    if (RegExp.test(req.originalUrl)) {
      const User = await sails.models.users.findOne({
        id: payload.id
      });

      if (User.position === 0) {
        return res.Forbidden();
      }
    }

    req.userID = payload.id;

    return proceed();
  }

  return res.Forbidden();

};