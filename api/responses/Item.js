/**
 * Item.js
 *
 * A custom response.
 *
 */

module.exports = async function Collection({
  data
}) {

  return this.res.send({
    code: 200,
    body: data
  });

};