/**
 * ExpiredToken.js
 *
 * A custom response.
 *
 */

module.exports = function ExpiredToken() {
  return this.res.send({
    error: true,
    code: 451,
    message: "Expired Token",
  });

};