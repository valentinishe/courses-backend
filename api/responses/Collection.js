/**
 * Collection.js
 *
 * A custom response.
 *
 */

module.exports = async function Collection({
  data,
  limit,
  page
}) {

  const sails = this.req._sails;

  return this.res.send({
    code: 200,
    body: await sails.helpers.pagination.with({
      data,
      limit,
      page
    })
  })

};