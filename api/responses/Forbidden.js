/**
 * Forbidden.js
 *
 * A custom response.
 *
 */



module.exports = function Forbidden() {

  return this.res.send({
    error: true,
    code: 403,
    message: "Forbidden"
  });
};