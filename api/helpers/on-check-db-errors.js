module.exports = {


  friendlyName: 'On check db errors',


  description: 'Error handling from the database',


  inputs: {
    data: {
      type: 'ref',
      required: true
    }
  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs, exits) {

    const Errors = {
      error: true,
      code: 422,
      validations: {}
    }
    if (_.get(inputs, 'data.code', false)) {
      switch (inputs.data.code) {
        case "E_UNIQUE":
          Errors.validations[inputs.data.attrNames] = sails.__('core.isUnique');
        default:
          null;
      }
    }
    if (Object.keys(Errors.validations).length) {
      return exits.success({ ...Errors
      });
    }
    console.warn('памилка ', inputs);
    return exits.success(false);

  }


};