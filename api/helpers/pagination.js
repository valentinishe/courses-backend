const LIMIT = 50;
const PAGE = 1;

module.exports = {


  friendlyName: 'Pagination',


  description: 'Pagination something.',


  inputs: {
    data: {
      type: 'ref',
      required: true
    },
    // sails: 
    limit: {
      type: 'number',
    },
    page: {
      type: 'number',
    }
  },


  exits: {
    success: {
      description: 'All done.',
    },

  },


  fn: async function ({
    data,
    limit,
    page,
  }, exits) {
    // console.log('this', this);
    const pagination = {
      page: page || PAGE,
      pages: 0,
      limit: limit || LIMIT,
      total: data.length,
      data: []
    };

    const newData = [
      []
    ];

    // console.log(`data`, Object.values(data), typeof (data));

    let index = 0;
    Object.keys(data).forEach(key => {
      console.log('len', newData[index]);
      if (newData[index].length >= limit) {
        newData.push([]);
        index++;
      }

      newData[index].push(data[key]);
    });

    pagination.data = newData[pagination.page - 1];
    // pagination.data = newData;
    pagination.pages = newData.length;

    return exits.success(pagination);






  }


};