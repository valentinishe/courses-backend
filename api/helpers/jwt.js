const jwt = require('jsonwebtoken');


function createToken({ payload, secret, expiresIn}){
    return jwt.sign( payload, secret, { expiresIn } );
}

async function verifyToken({ secret, token }){
  return await jwt.verify(token, secret, function(err, decoded) {    

    if(err === null){
      return decoded;
    } 

    if(err.name === 'TokenExpiredError'){
      return null;
    }

    if(err.name === 'JsonWebTokenError'){
      return false;
    }
    
    return false;
  });
}



module.exports = {


  friendlyName: 'Jwt',


  description: 'Jwt something.',


  inputs: {},


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs, exits) {
    return exits.success({
      createToken,
      verifyToken
    })
  }


};

