

methods = {
  isRequired: (value) => {
    return Boolean(value) || value === 0 ;
  },
  
  isNumber: (value) => {
    return typeof(+value) === 'number';
  },
  
  isLength: (min, max) => {
    return (value) => !Boolean((String(value).length < min || String(value).length > max) && String(value).length !== 0);
  },

  isMaxLength: (max) => {
    return (value) => !Boolean(String(value).length > max && String(value).length !== 0);
  },

  isMinLength: (min) => {
    return (value) => !Boolean(String(value).length < min && String(value).length !== 0);
  }
}


module.exports = {
  friendlyName: 'Validation methods',
  description: 'Validations methods',

  inputs: {},

  exits: {
    success: {
      description: 'All done.',
    },
  },


  fn: async function (inputs, exits) {
    return exits.success({ ...methods});
  }


};

