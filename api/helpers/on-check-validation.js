module.exports = {


  friendlyName: 'Check validation',


  description: '',


  inputs: {
    data: {
      type: 'ref',
      description: 'Data that needs to be checked',
      required: true
    },
    validationMap: {
      type: 'ref',
      description: 'Validation rules',
      required: true
    }
  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function ({
    data,
    validationMap
  }, exits) {

    const Errors = {
      error: true,
      code: 422,
      validations: {}
    };

    Object.keys(validationMap.methods).forEach(key => {
      const property = validationMap.methods[key];

      if (property) {
        Object.keys(property).forEach(method => {
          if (!validationMap.methods[key][method](data[key])) {
            Errors.validations[key] = validationMap.messages[key][method];
          }
        });
      }

    });

    if (Object.keys(Errors.validations).length) {
      return exits.success({ ...Errors
      });
    }

    return exits.success(false);
  }




};